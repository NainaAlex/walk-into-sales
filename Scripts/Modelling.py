#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 14:31:13 2021

@author: naina.alex1
"""

#%%[0]
import pandas as pd
import numpy as np
from imblearn.over_sampling import SMOTENC 
from sklearn.model_selection import train_test_split as tts
from sklearn.metrics import classification_report,confusion_matrix
from imblearn.combine import SMOTEENN 
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn import metrics

# Model packages

from sklearn.ensemble import RandomForestClassifier 
#from xgboost import XGBoostClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB


#%%[1]
File = pd.read_csv(r'/Users/naina.alex1/Desktop/Walk into Sales/Data for Modelling and EDA/Model_file.csv')
File['Year_of_Visit'] = File['Year_of_Visit'].replace([2016, 2017], np.nan)

File = File.dropna(subset = ["Year_of_Visit"])
File = File.reset_index()
File = File.drop('index', 1)

#%%[1.1]

Model_file = File[['budget', 'Day_of_Visit', 'Project',
                   'sector', 'unit_type', 'rent', 'company',
                   'home_to_work_dist', 'source_bucketing',
                   'income', 'age_group', 'current_residence_type',
                   'source', 'interested_in', 'people_in_family',
                   'Source of Fund',  'selfuse_inv', 'mode_of_commute',
                   'no_of_people_employed','business', 'home_to_project_dist',
                   'residential_status', 'dep_var']]

le = LabelEncoder()
                     
#%%[2]         
     
df_cat_encoded = Model_file[['budget', 'Project',
                            'sector', 'unit_type', 'rent', 'company',
                            'home_to_work_dist', 'source_bucketing',
                            'income', 'age_group', 'current_residence_type',
                            'source', 'interested_in',
                            'Source of Fund',  'selfuse_inv', 'mode_of_commute',
                            'no_of_people_employed','business', 
                            'residential_status']].apply(le.fit_transform)

#%%[3]
df_cat_encoded[['Day_of_Visit', 'home_to_project_dist', 'dep_var']] = Model_file[['Day_of_Visit', 'home_to_project_dist', 'dep_var']]

#%%[4]
fig = plt.figure(figsize=(10,8))
sb.heatmap(df_cat_encoded.drop('dep_var', axis = 1).corr())
plt.show()
   
#%%[5]
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
scaler.fit(df_cat_encoded.drop('dep_var', axis = 1)) #.drop['dep_var']
df1 = scaler.transform(df_cat_encoded.drop('dep_var', axis = 1))

df2 = pd.DataFrame(df1, columns = df_cat_encoded.drop('dep_var', axis = 1).columns)
df2['dep_var'] = df_cat_encoded['dep_var']              
                     
#%%[6]
X_train, X_test_temp, y_train, y_test_temp = tts(df2.drop('dep_var', axis = 1), df2['dep_var'], test_size = 0.4, random_state=101)

X_test, X_oot, y_test, y_oot = tts(X_test_temp, y_test_temp, test_size = 0.1, random_state=101)


#%%[7]
sme = SMOTEENN(sampling_strategy = 4/6, random_state=42)
X_res, y_res = sme.fit_resample(X_train, y_train)

#%%[8] ADABOOST CLASSIFIER with sampling

ad = AdaBoostClassifier()

cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
n_scores = cross_val_score(ad, X_res, y_res, scoring='accuracy', cv=cv, n_jobs=-1, error_score='raise')

# report performance
print('Accuracy: %.3f (%.3f)' % (np.mean(n_scores), np.std(n_scores)))

ad.fit(X_res, y_res)
pred = ad.predict(X_test)
print("Test")
pred = ad.predict(X_test)
print(classification_report(y_test,pred)) 

print("\n")

print("OOT")
oot_pred = ad.predict(X_oot)
print(classification_report(y_oot,oot_pred)) 



#%%[9]
from imblearn.under_sampling import NearMiss
undersample = NearMiss(version=2, n_neighbors=3)
X_und, y_und = undersample.fit_resample(X_train, y_train)

#%%[10] ADABOOST CLASSIFIER with under - sampling

ad_un = AdaBoostClassifier()

cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
n_scores = cross_val_score(ad_un, X_und, y_und, scoring='accuracy', cv=cv, n_jobs=-1, error_score='raise')

# report performance
print('Accuracy: %.3f (%.3f)' % (np.mean(n_scores), np.std(n_scores)))

ad_un.fit(X_und, y_und)
pred = ad_un.predict(X_test)
print("Test")
pred = ad_un.predict(X_test)
print(classification_report(y_test,pred)) 

cm1=confusion_matrix(y_test,pred)
sb.heatmap(cm1, annot=True, fmt = '0.2f')
plt.xlabel('Predicted Overall')
plt.ylabel('Actual')

print("\n")

print("OOT")
oot_pred = ad_un.predict(X_oot)
print(classification_report(y_oot,oot_pred)) 

#%%
rf = RandomForestClassifier(max_depth = 10, max_features = 'auto', random_state = 101, class_weight = {0:1, 1:2})
rf.fit(X_res, y_res)

pred_rf = rf.predict(X_test)
print("Test")
print(classification_report(y_test,pred_rf)) 

cm_1 = confusion_matrix(y_test,pred_rf)
sb.heatmap(cm_1, annot=True, fmt = '0.2f')
plt.xlabel('Predicted Overall')
plt.ylabel('Actual')
plt.title('RF with resampling')

print("\n")

print("OOT")
oot_pred_rf = rf.predict(X_oot)
print(classification_report(y_oot,oot_pred_rf)) 

#%%
rf_und = RandomForestClassifier(max_depth = 5, max_features = 5, random_state = 101) #, class_weight = {0:1, 1:2})
rf_und.fit(X_und, y_und)

pred_rf_und = rf_und.predict(X_test)
print("Test")
pred = rf_und.predict(X_test)
print(classification_report(y_test,pred_rf_und)) 

cm_2 = confusion_matrix(y_test,pred_rf_und)
sb.heatmap(cm_2, annot=True, fmt = '0.2f')
plt.xlabel('Predicted Overall')
plt.ylabel('Actual')
plt.title('RF with under sampling')

print("\n")

print("OOT")
oot_pred_rf_und = rf_und.predict(X_oot)
print(classification_report(y_oot,oot_pred_rf_und)) 

#%%
rf = RandomForestClassifier(max_depth = 8, random_state = 101, n_estimators = 100, class_weight = {0:1, 1:8}) #
rf.fit(X_train, y_train)

pred_rf = rf.predict(X_test)
print("Test")
print(classification_report(y_test,pred_rf)) 

cm_3 = confusion_matrix(y_test,pred_rf)
sb.heatmap(cm_3, annot=True, fmt = '0.2f')
plt.xlabel('Predicted Overall')
plt.ylabel('Actual')
plt.title('RF')

print("\n")
#%%
print("OOT")
oot_pred_rf = rf.predict(X_oot)
print(classification_report(y_oot,oot_pred_rf)) 

cm_4 = confusion_matrix(y_oot,oot_pred_rf)
sb.heatmap(cm_4, annot=True, fmt = '0.2f')
plt.xlabel('Predicted Overall')
plt.ylabel('Actual')
plt.title('RF')
#%%
def decile(X,y,model):
    df_deciles=pd.DataFrame(y)
    df_deciles['Probability']=model.predict_proba(X)[:,1]
    df_deciles['decile']=pd.qcut(df_deciles['Probability'],10,labels=['10', '9', '8', '7', '6', '5', '4', '3', '2','1'])
    df_deciles.columns = ['Buyers','Probability','Decile']
    df_deciles['Non-Buyers'] = 1-df_deciles['Buyers']

    df1 = pd.pivot_table(data=df_deciles,index=['Decile'],values=['Buyers','Non-Buyers','Probability'],
                     aggfunc={'Buyers':[np.sum],
                              'Non-Buyers':[np.sum],
                              'Probability' : [np.min,np.max]})
    
    df1.reset_index()
    df1.columns = ['Buyers_Count','Non-Buyers_Count','max_score','min_score']
    df1['Total_Cust'] = df1['Buyers_Count']+df1['Non-Buyers_Count']
    
    df2 = df1.sort_values(by='min_score',ascending=False)
    df2['Buyer_Rate'] = (df2['Buyers_Count'] / df2['Total_Cust']).apply('{0:.2%}'.format)
    df2['Non-Buyer_Rate'] = (df2['Non-Buyers_Count'] / df2['Total_Cust']).apply('{0:.2%}'.format)
    
    default_sum = df2['Buyers_Count'].sum()
    non_default_sum = df2['Non-Buyers_Count'].sum()
    df2['Buyers %'] = (df2['Buyers_Count']/default_sum).cumsum().apply('{0:.2%}'.format)
    df2['Non_Buyers %'] = (df2['Non-Buyers_Count']/non_default_sum).cumsum().apply('{0:.2%}'.format)
    
    df2['ks_stats'] = np.round(((df2['Buyers_Count'] / df2['Buyers_Count'].sum()).cumsum() -(df2['Non-Buyers_Count'] / df2['Non-Buyers_Count'].sum()).cumsum()), 4) * 100
    flag = lambda x: '*****' if x == df2['ks_stats'].max() else ''
    df2['max_ks'] = df2['ks_stats'].apply(flag)
    return df2

#%%
#decile(X_oot,y_oot,rf).to_csv(r'/Users/naina.alex1/Desktop/Walk into Sales/Data for Modelling and EDA/KS.csv')

#%%
def cut_off(model,X,y):
    predict_proba = pd.DataFrame(model.predict_proba(X))
    pred_log = model.predict(X)
    pred_log = pd.DataFrame(pred_log)
    Y1_test1 = y.reset_index()
    predictions = pd.concat([Y1_test1,pred_log,predict_proba],axis = 1)
    predictions.columns = ['index', 'actual', 'predicted', 'Purchased_0', 'Purchased_1']
    
    auc_score = metrics.roc_auc_score( predictions.actual, predictions.Purchased_1 )
    print(round( float( auc_score ), 2 ))
    
    fpr, tpr, threshold = metrics.roc_curve(y,predictions.Purchased_1,drop_intermediate=False)
    roc_auc = metrics.auc(fpr, tpr)

    plt.title('Receiver Operating Characteristic')
    plt.plot(fpr, tpr, 'b', label='ROC curve (area = %0.2f)' % auc_score)
    plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
#%%
y_file = pd.DataFrame(y_test)
y_file['pred'] = pred_rf
#%%
cut_off(rf, X_oot, y_oot)
#%%
import shap
explainer = shap.TreeExplainer(rf)
#%%
# Calculate Shap values
choosen_instance = X_test.loc[7936].round(2)
shap_values = explainer.shap_values(choosen_instance)
shap.initjs()
shap.force_plot(explainer.expected_value[1], shap_values[1], choosen_instance, matplotlib=True)
 
#%%
shap_values = shap.TreeExplainer(rf).shap_values(X_train)
shap.summary_plot(shap_values, X_train)
#%%

