#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 16:33:15 2021

@author: naina.alex1
"""

#%% [0] Libraries
import pandas as pd
import numpy as np
import matplotlib as plt
import seaborn as sb
import re

import datetime
from pandas.plotting import table
from dateutil import parser
import calendar
from geopy import distance

from geopy.geocoders import Nominatim
from geopy.distance import geodesic
from geopy.exc import GeocoderTimedOut

import folium
from folium import plugins

from imblearn.over_sampling import SMOTENC, SMOTE
from sklearn.metrics import classification_report,confusion_matrix
from sklearn.model_selection import train_test_split as tts
from sklearn.linear_model import LogisticRegression as LR
from sklearn.neighbors import KNeighborsClassifier as kNN
#import xgboost as xg
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.metrics import fbeta_score

#%%[1] Files
file_model_mod=pd.read_csv(r'/Users/naina.alex1/Desktop/Walk into Sales/Data for Modelling and EDA/file.csv')
file_model_oot=pd.read_csv(r'/Users/naina.alex1/Desktop/Walk into Sales/Data for Modelling and EDA/oot.csv')

#%%[2]Concatinate files for encoding and functions

file_model=pd.concat([file_model_mod,file_model_oot])

#%%[3] Cleaning of the File
business=list(file_model['business'].unique())
listing=['Yes','No','Nobody','nobody','none','None',np.nan]
for i in listing:
    if i in business:
        business.remove(i)
        
listing.remove('Yes')
listing.remove(np.nan)
file_model['business']=file_model['business'].replace(business,'Yes')
file_model['business']=file_model['business'].replace(listing,'No')


file_model['business_type']=file_model['business_type'].str.title()
file_model['business_type']=file_model['business_type'].replace(['Business','business','Government'],'Others')

file_model['mode_of_commute']=file_model['mode_of_commute'].str.title()
list_commute=list(file_model['mode_of_commute'].unique())
for i in list(['Car','Two Wheeler','Public Transport','Others',np.nan]):
    if i in list_commute:
        list_commute.remove(i)
file_model['mode_of_commute']=file_model['mode_of_commute'].replace(list_commute,'Others')

n_l=list(file_model['location'])
n_wl=list(file_model['work_location'])
file_model['location_latitude']=file_model['location_latitude'].astype(float)
file_model['location_longitude']=file_model['location_longitude'].astype(float)

file_model['work_location_latitude']=file_model['work_location_latitude'].astype(float)
file_model['work_location_longitude']=file_model['work_location_longitude'].astype(float)


file_model['no_of_people_employed']=file_model['no_of_people_employed'].replace(['01-Feb',1,2,'1','2'],'1-2')
file_model['no_of_people_employed']=file_model['no_of_people_employed'].replace('2-3','3-4')
file_model['no_of_people_employed']=file_model['no_of_people_employed'].replace('3','3-4')
file_model['no_of_people_employed']=file_model['no_of_people_employed'].replace(3,'3-4')
file_model['no_of_people_employed']=file_model['no_of_people_employed'].replace(4,'3-4')
file_model['no_of_people_employed']=file_model['no_of_people_employed'].replace('4','3-4')
file_model['no_of_people_employed']=file_model['no_of_people_employed'].replace(['4+',5,6,7,8,9],'5+')


married=list(file_model['marital_status'].unique())
married.remove('Single')
married.remove('Married')
married.remove(np.nan)
file_model['marital_status']=file_model['marital_status'].replace(married,'Others')


file_model['residential_status']=file_model['residential_status'].str.title()
res_status=list(file_model['residential_status'].unique())
for i in ['India','Indian',np.nan]:
    if i in res_status:
        res_status.remove(i)
file_model['residential_status']=file_model['residential_status'].replace(res_status,'Nri')
file_model['residential_status']=file_model['residential_status'].replace('India','Indian')


file_model['people_in_family']=file_model['people_in_family'].str.strip()
file_model['people_in_family']=file_model['people_in_family'].replace(['05-Jun','02-Mar','04-May','01-Feb','03-Apr'],['5-6','2-3','4-5','1-2','3-4'])


file_model['no_of_people_employed']=file_model['no_of_people_employed'].str.strip()

file_model['sector']=file_model['sector'].str.strip()
file_model['sector']=file_model['sector'].str.title()

file_model['income']=file_model['income'].str.strip()
file_model['income']=file_model['income'].replace(['2L - 3.5L', '3.5L - 5L'],'2L - 5L')

file_model['current_accommodation']=file_model['current_accommodation'].str.strip()
file_model['current_accommodation']=file_model['current_accommodation'].str.title()


file_model['rent']=file_model['rent'].str.strip()
file_model['rent']=file_model['rent'].str.title()
file_model['rent']=file_model['rent'].replace(['30000+','<10000'],['More Than 30000','Less Than 10000'])


file_model['current_residence_type']=file_model['current_residence_type'].str.strip()
file_model['current_residence_type']=file_model['current_residence_type'].str.title()
file_model['current_residence_type']=file_model['current_residence_type'].replace(['Pg','Purchasing For His Parents'],'Others')


file_model['Source of Fund']=file_model['Source of Fund'].replace(['Own: Funding by selling another property',
                                                                     'Own: Some funding himself with help from others (family member etc)',
                                                                    'Own: Complete funding on his own', 'Own: Completely funded by others (family member etc)'],'Own Source')

file_model['Source of Fund']=file_model['Source of Fund'].replace(['Own funds and Bank Loan'],'Bank/Own')
    

file_model['budget']=file_model['budget'].replace({'25 L - 50 L':'30 L - 40 L', '25 L - 40 L':'30 L - 40 L', 
                                                     '40 L - 50L':'40 L - 50 L'})


file_model['dosv']=pd.to_datetime(file_model['dosv'])
file_model['Month_of_Visit']=file_model['dosv'].dt.month
file_model['Year_of_Visit']=file_model['dosv'].dt.year
file_model['Day_of_Visit']=file_model['dosv'].dt.day_name()
file_model['Day_of_Visit']=file_model['Day_of_Visit'].replace({'Thursday':3, 'Sunday':6, 'Wednesday':2, 'Friday':4, 
                                                         'Saturday':5, 'Tuesday':1,'Monday':0})
#%% Using Coordinate data.

file_model['project_latitude']=2.556
file_model['project_longitude']=2.556

file_model.loc[file_model['Project']=='Green Enclave', 'project_latitude'] = 13.020336
file_model.loc[file_model['Project']=='Green Enclave', 'project_longitude'] = 80.129193

file_model.loc[file_model['Project']=='Green Acres Apartments', 'project_latitude'] = 12.875371
file_model.loc[file_model['Project']=='Green Acres Apartments', 'project_longitude'] = 80.107809

file_model.loc[file_model['Project']=='Light House', 'project_latitude'] = 12.953543
file_model.loc[file_model['Project']=='Light House', 'project_longitude'] = 80.162485

file_model.loc[file_model['Project']=='Flourish Apartments', 'project_latitude'] = 12.881582
file_model.loc[file_model['Project']=='Flourish Apartments', 'project_longitude'] = 80.006078

file_model.loc[file_model['Project']=='Karapakkam Villas', 'project_latitude'] = 12.911831
file_model.loc[file_model['Project']=='Karapakkam Villas', 'project_longitude'] = 80.234668

file_model.loc[file_model['Project']=='Peninsula', 'project_latitude'] = 13.013759
file_model.loc[file_model['Project']=='Peninsula', 'project_longitude'] = 80.160663

file_model.loc[file_model['Project']=='Hamlet', 'project_latitude'] = 12.911831
file_model.loc[file_model['Project']=='Hamlet', 'project_longitude'] = 80.234668

file_model.loc[file_model['Project']=='GA/GH Villas & Row Houses', 'project_latitude'] = 12.87933
file_model.loc[file_model['Project']=='GA/GH Villas & Row Houses', 'project_longitude'] = 80.109391

file_model.loc[file_model['Project']=='Flourish Plots', 'project_latitude'] = 12.87933
file_model.loc[file_model['Project']=='GFlourish Plots', 'project_longitude'] = 80.109391

##############################################

file_model['home_to_work_dist']=0
file_model['project_to_work_dist']=0
file_model['home_to_project_dist']=0
#%%
file_model = file_model.reset_index()
for i in range(len(file_model)):
    file_model['home_to_work_dist'][i] =  distance.distance((file_model['location_latitude'][i],file_model['location_longitude'][i]), (file_model['work_location_latitude'][i],file_model['work_location_longitude'][i])).km
    file_model['project_to_work_dist'][i] =  distance.distance((file_model['project_latitude'][i],file_model['project_longitude'][i]), (file_model['work_location_latitude'][i],file_model['work_location_longitude'][i])).km
    file_model['home_to_project_dist'][i] =  distance.distance((file_model['location_latitude'][i],file_model['location_longitude'][i]), (file_model['project_latitude'][i],file_model['project_longitude'][i])).km
#%%
#file_model.to_csv(r'/Users/naina.alex1/Downloads/Lead Scoring model/Model_data_new.csv')




