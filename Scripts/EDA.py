#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 14:31:29 2021

@author: naina.alex1
"""

#%%[0] Import required packages
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sb
import plotly as go
import sweetviz as sv

import os as os
from itertools import product
import scipy.stats as ss

from sklearn.preprocessing import LabelEncoder
from sklearn.feature_selection import chi2

import statsmodels.api as sm
from statsmodels.stats.outliers_influence import variance_inflation_factor
#%%[1] Import files
File = pd.read_csv(r'/Users/naina.alex1/Desktop/Walk into Sales/Data for Modelling and EDA/Model_file.csv')
#%%

File['dosv'] = pd.to_datetime(File['dosv'])
File['Month_Year'] = File['dosv'].dt.to_period("M")
#%%
file = File[['age_group', 'residential_status', 'people_in_family', 'kids_age',
       'no_of_people_employed', 'sector', 'company', 'budget', 'interested_in',
       'source', 'source_bucketing', 'income', 'current_accommodation', 'rent',
       'current_residence_type', 'selfuse_inv', 'reason_for_purchase',
       'Source of Fund', 'business', 'mode_of_commute',  'first_home', 'unit_type',
       'Project', 'mode_of_visit', 'Year_of_Visit', 'Day_of_Visit',
       'Month_of_booking', 'Year_of_booking','home_to_work_dist', 'Month_of_Visit', 
       'project_to_work_dist','home_to_project_dist', 'dep_var', 'Month_Year']]

#%%
#my_report = sv.analyze(file, target_feat = 'dep_var')
#my_report.show_html()
#%%
df_cat = pd.DataFrame(data = file.dtypes, columns = 
                                         ['a']).reset_index()
cat_var = list(df_cat['index'].loc[df_cat['a'] == 'object'])

df_cat = file[cat_var]
df_cat['Month_Year'] = file['Month_Year']

df_cat_v1 = df_cat.dropna()

## Let us split this list into two parts
cat_var1 = ('budget', 'age_group', 'no_of_people_employed', 'first_home',
       'interested_in', 'income', 'current_residence_type', 'selfuse_inv',
       'mode_of_visit', 'Source of Fund', 'Project')
cat_var2 = ('budget', 'age_group', 'no_of_people_employed', 'first_home',
       'interested_in', 'income', 'current_residence_type', 'selfuse_inv',
       'mode_of_visit', 'Source of Fund', 'Project')

## Let us jump to Chi-Square test
## Creating all possible combinations between the above two variables list
cat_var_prod = list(product(cat_var1,cat_var2, repeat = 1))

result_non = []
result = []
for i in cat_var_prod:
    if i[0] != i[1]:
        result.append((i[0],i[1],list(ss.chi2_contingency(pd.crosstab(
                                    df_cat_v1[i[0]], df_cat_v1[i[1]])))[1]))
        if (ss.chi2_contingency(pd.crosstab(df_cat_v1[i[0]], df_cat_v1[i[1]])))[1] > 0.05:
            result_non.append((i[0],i[1],list(ss.chi2_contingency(pd.crosstab(df_cat_v1[i[0]], df_cat_v1[i[1]])))[1]))

chi_test_output = pd.DataFrame(result, columns = ['var1', 'var2', 'coeff'])
                                                       
#%%Encoding cat var
le = LabelEncoder()
Encoded_cat = pd.DataFrame()
for i in df_cat_v1.columns:
    Encoded_cat[i] = le.fit_transform(df_cat_v1[i])

#%%
listing = []
for i in file.columns:
    if i not in Encoded_cat.columns:
        listing.append(i)
#%%

Encoded_cat[listing] = file[listing]
#%%
X = Encoded_cat.drop(['dep_var','Year_of_Visit'],axis=1)
y = Encoded_cat['dep_var']

#%%
chi_scores = chi2(X,y)

#%%
p_values = pd.Series(chi_scores[1],index = X.columns)
p_values.sort_values(ascending = False , inplace = True)
#%%
p_values.plot.bar()

#%%

vif_info = pd.DataFrame()
vif_info['VIF'] = [variance_inflation_factor(X.values, i) for i in range(X.shape[1])]

vif_info['Column'] = X.columns
vif_info = vif_info.sort_values('VIF', ascending=False)

#%%
X_new = Encoded_cat.drop(['dep_var','Year_of_Visit', 'Year_of_booking','Month_of_Visit', 'Month_of_booking','current_accommodation','project_to_work_dist',
                          'mode_of_visit', 'Month_Year','reason_for_purchase', 'kids_age', 'first_home'],axis=1)

#%%
vif_new = pd.DataFrame()
vif_new['VIF'] = [variance_inflation_factor(X_new.values, i) for i in range(X_new.shape[1])]

vif_new['Column'] = X_new.columns
vif_new = vif_new.sort_values('VIF', ascending=False)

#%%
Required_columns = list(vif_new.Column)
































